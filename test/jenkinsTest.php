<?php
include ("public/jenkins.php");

class jenkinsTest extends \PHPUnit_Framework_TestCase
{
    public function testGetText()
    {
        $text = 'text';
        $jenkins = new \jenkins();

        $jenkins->setText($text);

        $this->assertEquals($text, $jenkins->getText());
    }
}